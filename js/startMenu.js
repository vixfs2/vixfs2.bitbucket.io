function chooseLevelScreen() {
	const NewMenu = `
  	<div class="container start">
	  	<div class="d-flex flex-column bd-highlight mb-3 align-content-center">
				<div class="p-2 bd-highlight flex-fill backlineimg">
				  	<div class="d-flex flex-column bd-highlight mb-3 gameinfo text-center ">
				  		<div class="p-2 bd-highlight topmenu">
						  	<div class="d-flex flex-column bd-highlight mb-3 topmenu1">	
						  		<div class="p-2 bd-highlight">
						  			<p class="gamenumber2 ">Игра №7</p>
						  		</div>
						  		<div class="p-2 bd-highlight">
						  			<button class="bgamename">
						  				Super <br>puzzles
						  			</button>
						 		</div>
						 		<div class="p-2 bd-highlight">
						  			<p class="helponstart">Стань лучшим, собери все пазлы и<br> открой карточки с крутыми героями!</p>
						 		</div>
						 	</div>
						</div>
				 		<div class="p-2 bd-highlight startcardimgs">
				  			<img src="assets/menu/super.svg">
				 		</div>
					</div>
				</div>
				<div class="p-2 bd-highlight p2btn">
					<button class="button-start" onclick="generateHudGame()">Начнём!</button>
				</div>
		</div>
	</div>
	</div>
`
	$("#screen").html(NewMenu);
}

function generateHudGame() {
	generateLine();
	newGame();
};

chooseLevelScreen();