let timer_numb, timer_animate;
let fieldGame = { level: '', record: '', minPoints: '', maxPoints: '', points: '', time: '', progressBar: '' };
let dataGame = { level: '', editLevel: false, record: '', minPoints: '', maxPoints: '', points: '', time: '', delay_time: '' };

function initProgressLine() {
    $('#modalEndGame').modal('hide');

    generateLine();
    fieldGame.level = document.querySelector(".level");
    fieldGame.record = document.querySelector(".record");
    fieldGame.points = document.querySelector(".points");
    fieldGame.time = document.querySelector('.time');
    fieldGame.progressBar = document.querySelector(".progressBar");
    fieldGame.minPoints = document.querySelector(".min_points");
    fieldGame.maxPoints = document.querySelector(".max_points");

    //let initialData = getInitialDataServer();
    let initialData = getInitialDataServer();
    dataGame.level = initialData.startLevel;
    //dataGame.level = 9;
    dataGame.record = initialData.record;

    let dataLevel = getDataLevel(dataGame.level);
    dataGame.time = dataLevel.time;
    dataGame.points = 0;


    if (dataLevel.delay_time) {
        dataGame.delay_time = dataLevel.delay_time;
    } else {
        dataGame.delay_time = 0;
    }

    //dataGame.delay_time = 0;

    sizeX = dataLevel.sizeX;
    sizeY = dataLevel.sizeY;
    dataGame.minPoints = Math.floor((sizeX * sizeX) / 2);
    dataGame.maxPoints = sizeX * sizeX - 1;

    fieldGame.level.textContent = dataGame.level;
    fieldGame.record.textContent = dataGame.record;
    fieldGame.minPoints.textContent = dataGame.minPoints;
    fieldGame.maxPoints.textContent = dataGame.maxPoints;
    fieldGame.points.textContent = dataGame.points;



    if (sizeX >= 8) {
        sizeGridPuzzle = Math.floor(sizeX / 2);
        //sizeGridPuzzle = 2;
    } else {
        sizeGridPuzzle = sizeX;
    };
    timeForRembemer = dataLevel.delay_time * 1000;
    pointsForFound = dataLevel.pointsForFound;
    pointsForFail = dataLevel.pointsForFail;

    generateGame();
    //console.log('generategame in initprogressline');
    // startTimer(dataGame.time);
    //delay_timer();
}


const servers = {
    prod: "https://api.heygo.school",
    dev: "https://ih1608160.vds.myihor.ru"
}

function getInitialDataServer() {

    jQuery.ajaxSetup({ async: false });

    var parameters = Qs.parse(window.location.search, {
        ignoreQueryPrefix: true
    });

    let server = servers[parameters.source] || "https://api.heygo.school";


    let res = { "startLevel": 1, "record": 0, "progress": 0};

    if (parameters.token !== undefined) {
        $.get(`${server}/api/v0.1/courses-management/games?token=${parameters.token}`, function (data) {

        })
            .done(function (data) {
                zog("успешная загрузка данных с сервера");
                try {
                    let incoming = JSON.parse(data.data.properties);
                    
                    res.startLevel = incoming.startLevel || 1;
                    res.record = incoming.record || 0;
                    res.progress = incoming.progress || 0;
                } catch (e) { console.log(e) }
            })
            .fail(function () {
                zog("не смог загрузить данные с сервера")
            });
    } else {
        zog("Ошибка авторизации, прогресс не будет сохранен")
    }
    return res;
}
function setInitialDataServer(startLevel, record) {

    jQuery.ajaxSetup({ async: true });

    var parameters = Qs.parse(window.location.search, {
        ignoreQueryPrefix: true
    });
    let server = servers[parameters.source] || "https://api.heygo.school";
    let progress = Math.floor(10 * startLevel);

    if (parameters.token !== undefined) {
        $.ajax({
            url: `${server}/api/v0.1/courses-management/games?token=${parameters.token}`,
            type: 'PUT',

            headers: {
                'Content-Type': 'application/json'
            },

            data: JSON.stringify({
                "properties": `{'startLevel':${startLevel}, 'record':${record}, 'progress':${progress}, 'score':1  }`
            }),

            success: function (result) {
                console.log("Прогресс сохранен");
            },
            error: function () {
                console.log("Прогресс не сохранен");
            }
        });
    } else {
        zog("Ошибка авторизации, прогресс не будет сохранен");
    }
}

function delay_timer() {
    //console.log("delay_timer");
    let time_del = dataGame.delay_time * 1000;
    if (sizeX >= 8) {
        time_del *= 2;
    }
    fieldGame.time.textContent = '00:00';
    setTimeout(function () {
        startTimer(dataGame.time);
        return true;
    }, time_del);

}

function newSessionGame() {
    if ((sizeX < 8) || (currentPart == 0)) {
        dataGame.points = 0;
    };
    //console.log('newSessionGame');

    if (dataGame.editLevel) {

        setInitialDataServer(dataGame.level, dataGame.record);
        
        let dataLevel = getDataLevel(dataGame.level);

        sizeX = dataLevel.sizeX;
        sizeY = dataLevel.sizeY;

        dataGame.minPoints = Math.floor((sizeX * sizeX) / 2);
        dataGame.maxPoints = sizeX * sizeX - 1;
        dataGame.time = dataLevel.time;

        fieldGame.minPoints.textContent = dataGame.minPoints;
        fieldGame.maxPoints.textContent = dataGame.maxPoints;



        if (sizeX >= 8) {
            sizeGridPuzzle = Math.floor(sizeX / 2);
            //sizeGridPuzzle = 2;
        } else {
            sizeGridPuzzle = sizeX;
        };
        timeForRembemer = dataLevel.delay_time * 1000;
        pointsForFound = dataLevel.pointsForFound;
        pointsForFail = dataLevel.pointsForFail;

        if (dataLevel.delay_time) {
            dataGame.delay_time = dataLevel.delay_time;
        } else {
            dataGame.delay_time = 0;
        }

        dataGame.editLevel = false;
    }
    fieldGame.points.textContent = dataGame.points;

    // startTimer(dataGame.time);
    //_____________________________________________________________________________________________________________________________________________________________
    if ((sizeX < 8) || (currentPart == 0)) {
        generateGame();
        //console.log('generategame in ifx8');
        //delay_timer();
    } else {
        generateGame.StartGame();
    }
    //_____________________________________________________________________________________________________________________________________________________________

}
function startTimer(time) {
    let minutes, seconds, diff,
        intervalVal = 20,
        widthProgress = 100,
        start = Date.now(),
        percentVal = (intervalVal * 100) / (time * 1000);

    function timer() {
        diff = time - (((Date.now() - start) / 1000) | 0);
        if (diff <= 0) {
            fieldGame.time.textContent = '00:00';
            endTimer(true, time);
            return;
        }
        minutes = (diff / 60) | 0;
        seconds = (diff % 60) | 0;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        fieldGame.time.textContent = minutes + ":" + seconds;

    }

    function timer_line() {
        let timePassed = Date.now() - start;
        if (timePassed >= (time * 1000)) {
            fieldGame.progressBar.querySelector('div').style.width = "0";
            return;
        }
        //widthProgress = Math.floor((widthProgress - percentVal)*1000)/1000;
        widthProgress = 100 * (1 - (timePassed / 1000) / time);
        fieldGame.progressBar.querySelector('div').style.width = widthProgress + "%";
    }
    timer();
    timer_numb = setInterval(timer, 1000);
    timer_animate = setInterval(timer_line, intervalVal);
}

function endTimer(endTimeAnswer = false, time) {
    clearInterval(timer_numb);
    clearInterval(timer_animate);
    if (endTimeAnswer) {
        let widthProgress = 0;
        let percentVal = (20 * 100) / 1000;
        let line_anim = setInterval(function () {
            widthProgress = widthProgress + percentVal;
            if (widthProgress > 100) {
                clearInterval(line_anim);
                changeLevelState(true);
                return;
            }
            fieldGame.progressBar.querySelector('div').style.width = widthProgress + "%";
        }, 20);

    } else {
        fieldGame.progressBar.querySelector('div').style.width = "100%";
    }
}
/**
 * @param {number} points - кол-во очков за ответ
 */

function changePoints(points) {
    dataGame.points += points;
    fieldGame.points.textContent = dataGame.points;
}


/**
 * @param {boolean} progress - Правильность ответа
 */
function changeLevelState(timeEnd = false) {
    let progress;
    let time_del = dataGame.delay_time * 1000;


    if (timeEnd) {
        active(verticalPieces, horizontalPieces, false, true);
        if (dataGame.points < dataGame.minPoints) progress = -1;
        else if (dataGame.points > dataGame.maxPoints) progress = 1;
        else progress = 0;
        //progress = -1;
        currentPart = 0;
    } else {
        if ((dataGame.points < dataGame.minPoints) && ((sizeX < 8) || (currentPart == 3))) progress = -1;
        else if ((dataGame.points > dataGame.maxPoints) && ((sizeX < 8) || (currentPart == 3))) progress = 1;
        else progress = 0;
        if (sizeX >= 8) {
            currentPart++;
        }

    }

    let active_wrong_answer = [], active_correct_answer = [],
        wrong_answers = document.getElementsByClassName('wrong_answer'),
        correct_answers = document.getElementsByClassName('correct_answer');
    for (let i = 0; i < 3; i++) {
        if (wrong_answers[i].classList.contains('active')) active_wrong_answer.push(wrong_answers[i]);
        if (correct_answers[i].classList.contains('active')) active_correct_answer.push(correct_answers[i]);
    }

    if (progress == 1) { //если очков больше max очков 
        if (active_wrong_answer.length != 0) {
            active_wrong_answer[0].classList.remove('active');
        } else {
            if ((active_correct_answer.length == 2) && (dataGame.level != dataLevels.length)) {
                    dataGame.level++;
                    dataGame.editLevel = true;
                    anim_level(true);
            } else if (active_correct_answer.length == 3){
                console.log('Последний уровень был пройден');
            } else {
                correct_answers[active_correct_answer.length].classList.add('active');
            } 
        }
    } else if (progress == -1) {  //если очков меньше min очков
        if (active_correct_answer.length != 0) {
            active_correct_answer[active_correct_answer.length - 1].classList.remove('active');
        }
        else {
            if (active_wrong_answer.length == 2) {
                if (dataGame.level != 1) {
                    dataGame.level--;
                    dataGame.editLevel = true;
                    anim_level(false);
                } else {
                    wrong_answers[0].classList.add('active');
                    active_wrong_answer[2] = wrong_answers[0];
                }
            } else if (active_wrong_answer.length != 3) {
                wrong_answers[2 - active_wrong_answer.length].classList.add('active');
            }
        }
    }

    function anim_level(bool) {
        fieldGame.level.style.opacity = '0';
        if (bool) {
            correct_answers[2].classList.add('active');
            active_correct_answer[2] = correct_answers[2];
        } else {
            wrong_answers[0].classList.add('active');
            active_wrong_answer[2] = wrong_answers[0];
        }

        setTimeout(function () {
            for (let j = 0; j < 3; j++) {
                if (bool) active_correct_answer[j].classList.remove('active');
                else active_wrong_answer[j].classList.remove('active');
            }
        }, 500);
        setTimeout(function () {
            fieldGame.level.textContent = dataGame.level;
            fieldGame.level.style.opacity = '1';
        }, 500);
    }

    setTimeout(function () {
        if (dataGame.points > dataGame.record) {
            dataGame.record = dataGame.points;
            fieldGame.record.textContent = dataGame.record;
            setInitialDataServer(dataGame.level, dataGame.record);
        }
    }, 100);


    //______ end session game
    if ((sizeX < 8) || (currentPart == 4)) {
        endTimer();
        //console.log("endTimer");
        currentPart=0;
        randomMainImg();
    };
    setTimeout(newSessionGame, 3000);
    //_______________________

}

/*function endGame() {
    endTimer();
    document.querySelector('.button_correct_answer').disabled = true;
    document.querySelector('.button_wrong_answer').disabled = true;
    document.querySelector('.button_end_session').disabled = true;
    $('#modalEndGame').modal('show')
}*/