let width = $("#screen").width(),
    height = $("#screen").height() - 5, // -5 ????????
    start = false,
    firstStart = true,
    heightLowerMenu = 100, enable = true, roll = true, beforeX, beforeY,
    imageObj = [], mainImage,
    stage, label, statePuzzle = 0,
    puzzleX,
    puzzleY,
    horizontalPieces, verticalPieces, colorMenu = "#f5f2f5",
    piecesArrayObj = [],
    stageW,
    stageH,
    changeSizeImageHeight = 0,
    changeSizeImageWidth = 0,
    pieceWidth,
    pieceHeight,
    indentImage = 0,
    coeff = 1, coeff2 = 1, watch = false,
    imageWidth,
    imageHeight,
    initialValueWidth, initialValueHeight, whitebackground,
    margin = 10, marginPuzzleInMenu = 50, // Отступы картинки от краев экрана и пазлов в меню
    footer, leftArrowBackground, rightArrowBackground, containerArrow,
    sizeMenu, sizeMenuPuzzle = heightLowerMenu * 0.5,
    menuObject = [], elem = {},
    marginMenu, inFuild = false,
    imageLeft, imageRight, imageWatch, imageLeftRightSize = 20, sizeWatch = 20,
    marginFirstElem = 35 + imageLeftRightSize,
    margin_left_menu = imageLeftRightSize + 30,
    imgMainPuzzle,
    currentMainImgId = 0, setRoll = false, setType = false,
    currentPart = 0,
    sizeGridPuzzle,
    numgamefan = 0,
    imageLength = 59, // Количество изображений
    countMenuPuzzle;

function randomMainImg(){
        randomMainImgId = Math.floor(Math.random() * imageLength);
        if (currentMainImgId == randomMainImgId) {
            randomMainImg(currentMainImgId);
        } else{
            currentMainImgId = randomMainImgId;
        }
    }
let randomMainImgId;
randomMainImg();

function generateMainImage(){
    if(sizeX >= 8){
        //imgMainPuzzle = allMainImgs[currentMainImgId].part[currentPart];
        imgMainPuzzle = currentMainImgId + "/image_part_00" + (currentPart + 1) + ".jpg";
    } else{
        //imgMainPuzzle = allMainImgs[currentMainImgId].full;
        imgMainPuzzle = currentMainImgId + "/full.jpg";
    }
}

/*function playSound(src) {
    var audio = new Audio();
    audio.src = src;
    audio.play();
}*/

function updateSizeGame() {
    width = $("#screen").width();
    height = $("#screen").height();
    widthLowerMenu = width;
    if (start) {
        if (Math.round((width - marginFirstElem) / (sizeMenuPuzzle + marginMenu)) <= countMenuPuzzle) {
        	 widthLowerMenu += (sizeMenuPuzzle + marginMenu) * (menuObject.length - countMenuPuzzle - 1);
             if (footer != undefined)
        	   footer.regX = 0;
    	} else {
    		widthLowerMenu += (sizeMenuPuzzle + marginMenu) * (menuObject.length - Math.round((width - marginFirstElem) / (sizeMenuPuzzle + marginMenu)));
            if (footer != undefined)
    		  footer.regX = 0;
    	}
        
        document.getElementById('holderCanvas').width = $("#screen").width();
        document.getElementById('holderCanvas').height = $("#screen").height() - 5;
        footer.y = height - heightLowerMenu;
        imageRight.y = height - heightLowerMenu + (heightLowerMenu - imageLeftRightSize)/2;
        imageLeft.y = height - heightLowerMenu + (heightLowerMenu - imageLeftRightSize) / 2;
        imageRight.x = width - imageLeftRightSize;
        resizeObject(width - margin * 2, height - heightLowerMenu - margin * 2 - sizeWatch, stage, margin, sizeWatch);
    }
}

function createArrow(name, x, y) {
    // Создание заднего фона у стрелок
    name.graphics.beginFill(colorMenu).drawRect(0, 0, 40, heightLowerMenu);
    name.x = x;
    name.y = y;
    name.addTo(containerArrow);
    containerArrow.setChildIndex(name, 0);
}

window.addEventListener('resize', updateSizeGame);


function showLoading(){
    document.getElementById("loader").style.display = "block";
}

function hideLoading(){
    document.getElementById("loader").style.display = "none";
}


function generateGame() {
    createjs.Sound.removeAllSounds();
    $("#screen").html('<div id="holder"></div>');
    //zog("запуск");
    showLoading();
    randomMainImg();
    generateMainImage();
    // SCALING OPTIONS
    // scaling can have values as follows with full being the default
    // "fit"    sets canvas and stage to dimensions and scales to fit inside window size
    // "outside"    sets canvas and stage to dimensions and scales to fit outside window size
    // "full"   sets stage to window size with no scaling
    // "tagID"  add canvas to HTML tag of ID - set to dimensions if provided - no scaling

    let scaling = "holder"; // this will resize to fit inside the screen dimensions

    let countPieces = 0;
    let totalPieces = 0;
    // as of ZIM 5.5.0 you do not need to put zim before ZIM functions and classes
    frame = new Frame(scaling, width, height);
    
    frame.on("ready", function () {
        //zog("ready from ZIM Frame"); // logs in console (F12 - choose console)

        stage = frame.stage;
        stageW = frame.width;
        stageH = frame.height;

        frame.outerColor = "#e4e4e4"; // Цвет за пределами холста
        frame.color = "#dbdbdb"; // Цвет внутри холста

        let con = new Container;

        footer = new Container;
        footer.y = height - heightLowerMenu;

        containerArrow = new Container;
        // with chaining - can also assign to a variable for later access

        //frame.loadAssets(["left.png", "right.png", "watch.png"], "assets/");

        if(sizeX>=8){
            frame.loadAssets([
                currentMainImgId + "/image_part_001.jpg", 
                currentMainImgId + "/image_part_002.jpg", 
                currentMainImgId + "/image_part_003.jpg", 
                currentMainImgId + "/image_part_004.jpg",
                currentMainImgId + "/full.jpg",
                "left.png", 
                "right.png", 
                "watch.png",
                "unwatch.png",
                "click.mp3",
                "fanfare.mp3",
                "fanfare2.mp3",
                "fanfare3.mp3",
                "letGo.mp3"], "https://168829.selcdn.ru/hey.go/Trainers/Puzzle/");
        } else{
            frame.loadAssets([
                currentMainImgId + "/full.jpg",
                "left.png", 
                "right.png", 
                "watch.png",
                "unwatch.png",
                "click.mp3",
                "fanfare.mp3",
                "fanfare2.mp3",
                "fanfare3.mp3",
                "letGo.mp3"], "https://168829.selcdn.ru/hey.go/Trainers/Puzzle/");     
        };

        frame.on("complete", function () {
            //zog("asdfasdfasdfadfadf");
            hideLoading();
            delay_timer();
            StartGame();
        });


        function StartGame() {
            let soundInstance;
            let soundClick = frame.asset("click.mp3");
            let soundLetGo = frame.asset("letGo.mp3");
            //zog(soundClick, soundLetGo);
            let soundFanfare;
            switch(numgamefan){
                case 0:
                    soundFanfare = frame.asset("fanfare.mp3");
                    numgamefan = 1;
                    break;
                case 1:
                    soundFanfare = frame.asset("fanfare2.mp3");
                    numgamefan = 2;
                    break;
                case 2:
                    soundFanfare = frame.asset("fanfare3.mp3");
                    numgamefan = 0;
                    break;
            }

            if (typeof mainImage != "undefined")
                mainImage.alpha = 0;
            generateMainImage();
            countPieces = 0;
            con.removeAllChildren();
            footer.removeAllChildren();
            containerArrow.removeAllChildren();
            stage.update();
            horizontalPieces = sizeGridPuzzle;
            verticalPieces = sizeGridPuzzle;

            if (marginFirstElem + (horizontalPieces * verticalPieces) * sizeMenuPuzzle + (horizontalPieces * verticalPieces) * marginPuzzleInMenu < width) {
                marginMenu = (width - (horizontalPieces * verticalPieces) * sizeMenuPuzzle - marginFirstElem) / horizontalPieces / verticalPieces;
                sizeMenu = width;
            }
            else {
                marginMenu = marginPuzzleInMenu;
                sizeMenu = marginFirstElem + (horizontalPieces * verticalPieces) * sizeMenuPuzzle + (horizontalPieces * verticalPieces) * marginPuzzleInMenu + imageLeftRightSize;
            }

                menuObject = [];

            lowerMenu(heightLowerMenu, width, footer, stage);
            footer.regX = 0;

            imageLeft = frame.asset("left.png");
            imageRight = frame.asset("right.png");
            imageWatch = frame.asset("watch.png").clone();
            imageObj = frame.asset(imgMainPuzzle).clone();
            widthLowerMenu = width;
            //Событие при клике
            imageRight.on("click", function(evt) {
                let reg = footer.regX;
                reg += widthLowerMenu;
                if (reg > sizeMenu - widthLowerMenu) 
                    reg = sizeMenu - widthLowerMenu;
                footer.regX = reg;
                stage.update();
                //zog("тута");
            });

            //console.log(dataLevels[level - 1].lvlTurn);
            //console.log(statePuzzle);
            if (dataLevels[level - 1].lvlTurn[statePuzzle] == 1) 
            	setRoll = true;
            else 
                setRoll = false;
                
            if (dataLevels[level - 1].lvlTypePuzzle[statePuzzle] == 1) 
            	setType = true;
            else 
            	setType = false;

            if((sizeX<8)||(currentPart==3)){
                statePuzzle = (statePuzzle + 1) % 3;
            };
            
            
            //Событие при клике
            imageLeft.on("click", function(evt) {
                let reg = footer.regX;
                reg -= width;
                if (reg < 0) 
                    reg = 0;
                footer.regX = reg;
                stage.update();
                //zog("тута");
            });

            //Событие при клике
            imageWatch.on("click", function(evt) {
                
                if (watch) {
                    active(verticalPieces, horizontalPieces, true, true);
                    imageWatch.image.src = "https://168829.selcdn.ru/hey.go/Trainers/Puzzle/watch.png";
                    //imageObj.alpha = 0;
                    imageObj.animate({alpha: 0},500);
                    con.setChildIndex(imageObj, 0);
                    imageWatch.y = 6.7;
                    //zog(imageWatch.image.src);
                    watch = false;
                }
                else {
                    active(verticalPieces, horizontalPieces, false, false);
                    imageWatch.image.src = "https://168829.selcdn.ru/hey.go/Trainers/Puzzle/unwatch.png";
                    //imageObj.alpha = 1;
                    imageObj.animate({alpha: 1},500);
                    imageWatch.y = 3;
                    con.setChildIndex(imageObj, con.getNumChildren()-1);
                    watch = true;
                }
                stage.update();
            });

            footer.y = height - heightLowerMenu;
            footer.visible = false;
            imageLeft.visible = false;
            imageWatch.visible = false;
            imageRight.visible = false;

            function setImage(imageObj) {
                if (width > height - heightLowerMenu) {
                    imageObj.height = height - heightLowerMenu - margin * 2 - sizeWatch;
                } else {
                    imageObj.width = width - margin * 2;
                }
            }

            setImage(imageObj);

            imageObj.addTo(con);
            containerArrow.addChild(imageLeft);
            containerArrow.addChild(imageRight);
            imageWatch.addTo(con);

            containerArrow.addTo(stage);
            // Размер и координаты стрелок и показа картинок
            imageLeft.height = imageLeftRightSize;
            imageRight.height = imageLeftRightSize;
            imageRight.y = height - heightLowerMenu + (heightLowerMenu - imageLeftRightSize)/2;
            imageLeft.y = height - heightLowerMenu + (heightLowerMenu - imageLeftRightSize) / 2;
            imageLeft.x = 15;
            imageRight.x = width - imageLeftRightSize;
            imageWatch.x = width / 2 + imageObj.width / 2 - sizeWatch * 2;
            imageWatch.y = 6.7;

            //imageObj.alpha = 1;
            imageObj.animate({alpha: 1},1000);
            imageLeft.image.crossOrigin = "Anonymous";
            imageRight.image.crossOrigin = "Anonymous";
            imageObj.image.crossOrigin = "Anonymous";
            start = true;
            initialValueWidth = imageObj.width;
            initialValueHeight = imageObj.height;

            let piecesArray = new Array();

            imageWidth = imageObj.width;
            imageHeight = imageObj.height;
            pieceWidth = imageWidth / horizontalPieces;
            pieceHeight = imageHeight / verticalPieces;
            changeSizeImageHeight = pieceHeight;
            changeSizeImageWidth = pieceWidth;
            // 
            let gap = 40;
            totalPieces = horizontalPieces * verticalPieces;

             // Белый фон
            whitebackground = new Shape();
            whitebackground.graphics.beginFill(colorMenu).drawRect(0, 0, imageObj.width, imageObj.height);

            //Расположение пазла
            puzzleX = (width - imageWidth) / 2;
            puzzleY = (height - imageHeight - heightLowerMenu) / 2;
            imageObj.pos(puzzleX, puzzleY);
            imageObj.alpha = 0;
            con.addTo(stage);
            stage.update();
            imageObj.animate({alpha: 1},1000);

            if (currentPart == 0 && sizeX >= 8) {
                mainImage = frame.asset(currentMainImgId + "/full.jpg").clone();
                mainImage.animate({alpha: 1},1000);
                setImage(mainImage);
                mainImage.addTo(con);
                mainImage.image.crossOrigin = "Anonymous";
                mainImage.pos(puzzleX, puzzleY);
                setTimeout(hideImage, 1000 * dataLevels[level - 1].delay_time);
            }
            else {
                setTimeout(generate, 1000 * dataLevels[level - 1].delay_time);
            }

            function hideImage() {
                mainImage.animate({alpha: 0},1000);
                setTimeout(generate, 1000 * dataLevels[level - 1].delay_time);
                stage.update();
            }
            

            function generate() {
                footer.visible = true;
                //imageObj.alpha = 0;
                imageObj.animate({alpha: 0},1000);
                imageLeft.visible = true;
                imageRight.visible = true;
                imageWatch.visible = true;
                // Задний фон стрелок
                leftArrowBackground = new Shape();
                rightArrowBackground = new Shape();
                createArrow(leftArrowBackground, 0, height - heightLowerMenu);
                createArrow(rightArrowBackground, width - 40, height - heightLowerMenu);
                whitebackground.x = puzzleX;
                whitebackground.y = puzzleY;
                stage.addChild(whitebackground);
                stage.setChildIndex(whitebackground, 0);
                stage.update();
            }

            countMenuPuzzle = verticalPieces * horizontalPieces - 1;
            
            for (j = 0; j < verticalPieces; j++) {
                piecesArrayObj[j] = [];
                for (i = 0; i < horizontalPieces; i++) {
                    let n = j + i * verticalPieces;

                    let offsetX = pieceWidth * i;
                    let offsetY = pieceHeight * j;


                    let x8 = pieceWidth / 8;
                    let y8 = pieceHeight / 8;

                    piecesArrayObj[j][i] = new Object();
                    piecesArrayObj[j][i].right = Math.floor(Math.random() * 2);
                    piecesArrayObj[j][i].down = Math.floor(Math.random() * 2);

                    if (j > 0) {
                        piecesArrayObj[j][i].up = 1 - piecesArrayObj[j - 1][i].down;
                    }
                    if (i > 0) {
                        piecesArrayObj[j][i].left = 1 - piecesArrayObj[j][i - 1].right;
                    }

                    piecesArray[n] = new Rectangle({
                        width: pieceWidth,
                        height: pieceHeight,

                    });

                    piecesArrayObj[j][i].state = false;

                    let tileObj = piecesArrayObj[j][i];
                    let s = new Shape;

                    // Перетаскивание мыши
                    let context = s.graphics;
                    s.drag();
                    piecesArrayObj[j][i].obj = s;
                    s.mouseChildren = false;
                    piecesArrayObj[j][i].id = s.id;

                    s.x = puzzleX;
                    s.y = puzzleY;

                    s.i = i;
                    s.j = j;

                    s.roll = 0;
                    let previous = {};
                    let timerInGame = false;
                    // При нажатии
                    s.addEventListener("mousedown", function (e) {
                        timerInGame = false;
                        let mc = e.currentTarget;
                        let xx = mc.x;
                        let yy = mc.y;
                        //playSound("sounds/click.mp3");
                        soundInstance = soundClick.play();
                        function timerGame() {
                            timerInGame = true;
                        }

                        
                        if (setRoll) {
                        	let timerId = setTimeout(timerGame, 130);
                        }
                        else timerInGame = true;

                        previous.x = xx;
                        previous.y = yy;
                        previous.right = false;

                        if (mc.location != "menu") {

                            inFuild = true;
                            let freeSpace = space(mc);

                            let lineX, lineY;
                            lineX = Math.round((mc.x - puzzleX) / changeSizeImageWidth) - (freeSpace.faultLeft / changeSizeImageWidth);
                            lineY = Math.round((mc.y - puzzleY) / changeSizeImageHeight) - (freeSpace.faultTop / changeSizeImageHeight);

                            previous.lineX = lineX;
                            previous.lineY = lineY;
                            previous.full = piecesArrayObj[freeSpace.Top][freeSpace.Left].full;
                            previous.faultLeft = freeSpace.faultLeft;
                            previous.faultTop = freeSpace.faultTop;


                            if (lineX < horizontalPieces && lineX >= 0 && lineY < verticalPieces && lineY >= 0 && mc.fuild == true) {
                                piecesArrayObj[lineX][lineY].state = false;
                                if (lineX == freeSpace.Left && lineY == freeSpace.Top && mc.roll == 0) {
                                    previous.right = true;
                                    //countPieces--;
                                    if (mc.roll == 0){
                                        countPieces--;
                                        changePoints(-1);
                                    }
                                }
                                //label.text = "Пазлы " + countPieces + "/" + totalPieces;
                                //label.text = '';
                            }
                        }
                        else {
                            beforeX = xx;
                            beforeY = yy;
                        }

                    });

                    function space(mc) {
                        let Left, Top, faultLeft = 0,
                            faultTop = 0;
                        top:
                            for (ii = 0; ii < verticalPieces; ii++)
                                for (jj = 0; jj < horizontalPieces; jj++) {
                                    Left = jj;
                                    Top = ii;
                                    if (piecesArrayObj[ii][jj].id == mc.id) break top;
                                }

                        if (mc.roll == 1) faultLeft = changeSizeImageWidth;
                        if (mc.roll == 3) faultTop = changeSizeImageHeight;
                        if (mc.roll == 2) {
                            faultLeft = changeSizeImageWidth;
                            faultTop = changeSizeImageHeight;
                        }

                        return {
                            Left: Left,
                            Top: Top,
                            faultLeft: faultLeft,
                            faultTop: faultTop
                        };
                    }

                    function findElement(id) {
                        for (i = 0; i < verticalPieces; i++)
                            for (j = 0; j < horizontalPieces; j++) 
                                if (piecesArrayObj[i][j].id == id) return piecesArrayObj[i][j];
                    }

                    function endRound() {
                        if((sizeX < 8) || (sizeX>=8) && (currentPart == 3)){
                            soundInstance = soundFanfare.play();
                        }
                            //level++;
                            //changePoints(500);
                            active(verticalPieces, horizontalPieces, false, true);
                            //StartGame();
                            //zog(sizeX, currentPart);
                            if (sizeX >= 8 && currentPart == 3) {
                                mainImage = frame.asset(currentMainImgId + "/full.jpg").clone();
                                setImage(mainImage);
                                mainImage.addTo(con);
                                mainImage.image.crossOrigin = "Anonymous";
                                mainImage.pos(puzzleX, puzzleY);
                                mainImage.alpha = 0;
                                mainImage.animate({alpha: 1},1000);
                                //zog(mainImage, mainImage.alpha);
                                stage.update();
                            }
                            
                            changeLevelState();
                            
                            
                    }

                    // При перетаскивании
                    s.addEventListener("pressmove", function (e) {
                        let mc = e.currentTarget;
                        if (e.stageY < height - heightLowerMenu * 2 / 3) {
                            enable = false;
                        }
                        else {
                            if (enable) {
                                mc.x = beforeX;
                                mc.y = beforeY;
                            }
                        }
                        // Увеличить
                        if (e.stageY < height - heightLowerMenu) {
                            menuObject[mc.j * verticalPieces + mc.i].location = "fuild";
                            mc.scaleX = coeff;
                            mc.scaleY = coeff;
                        }
                        else {
                            //уменьшить
                            menuObject[mc.j * verticalPieces + mc.i].location = "menu";
                            mc.scaleX = sizeMenuPuzzle / pieceWidth;
                            mc.scaleY = sizeMenuPuzzle / pieceWidth;
                            if (mc.location == "fuild") {
                                if (mc.roll == 1) mc.x -= sizeMenuPuzzle/2;
                                if (mc.roll == 3) mc.y -= sizeMenuPuzzle/2;
                                if (mc.roll == 2) {
                                    mc.x -= sizeMenuPuzzle/2;
                                    mc.y -= sizeMenuPuzzle/2;
                                }
                            }
                        }
                        stage.update();
                    });

                    // Отпустить элемет
                    s.addEventListener("pressup", function (e) {

                        let mc = e.currentTarget;

                        let xx = mc.x;
                        let yy = mc.y;
                        isMenu = false;
                        let last = {}; 
                        

                        if (mc.location == "menu") {
                            if (e.stageY < height - heightLowerMenu) {
                                last.x = beforeX;
                                last.y = beforeY;
                                last.id = mc.j * verticalPieces + mc.i;

                                mc.location = "fuild";
                                menuObject[mc.j * verticalPieces + mc.i].location = "fuild";
                                
                                let x = mc.x, y = mc.y;
                                elem = findElement(mc.id);
                                footer.removeChild(mc);
                                con.addChild(mc);
                                let id = mc.j + mc.i * verticalPieces;
                                mc.y = y + (height - heightLowerMenu);
                                xx = Math.round(mc.x - footer.regX);
                                yy = Math.round(mc.y);
                                if (yy > height - heightLowerMenu)
                                    yy -= height - heightLowerMenu;
                                updatePuzzleSize(mc.j, mc.i);
                                stage.update();
                                timerInGame = true;
                                isMenu = true;
                                elementInFuild(id);
                            }
                            else {
                                    mc.x = beforeX;
                                    mc.y = beforeY;
                                    mc.scaleX = sizeMenuPuzzle / pieceWidth;
                                    mc.scaleY = sizeMenuPuzzle / pieceWidth;
                            }
                        }
                        else {
                            if (e.stageY >= height - heightLowerMenu) {
                                mc.location = "menu";
                                mc.scaleX = sizeMenuPuzzle / pieceWidth;
                                mc.scaleY = sizeMenuPuzzle / pieceWidth;
                                menuObject[mc.j * verticalPieces + mc.i].location = "menu";
                                let id = mc.j + mc.i * verticalPieces;
                                con.removeChild(mc);
                                footer.addChild(mc);
                                addPuzzleInMenu(mc, id);
                            }
                            else {
                                elementInFuild();
                            }
                        }
                        stage.update();
                        inFuild = false;
                        enable = true;

                        function elementInFuild(id) {
                            //clearTimeout(timerId);
                            if (timerInGame == true) {
                                let faultLeft = 0,
                                    faultTop = 0;

                                let freeSpace = space(mc); // Сколько ячеек слева и сверху от текущей и погрешность координат от поворота фигуры

                                let puzzleTop, puzzleLeft, puzzleRight, puzzleBottom;


                                puzzleLeft = puzzleX - changeSizeImageWidth / 2 + freeSpace.faultLeft;
                                puzzleRight = puzzleX - changeSizeImageWidth / 2 + imageWidth * coeff + freeSpace.faultLeft;
                                puzzleTop = puzzleY - changeSizeImageHeight / 2 + freeSpace.faultTop;
                                puzzleBottom = puzzleY - changeSizeImageHeight / 2 + imageHeight * coeff + freeSpace.faultTop;

                                if (xx > puzzleLeft && xx < puzzleRight && yy > puzzleTop && yy < puzzleBottom) {
                                    mc.x = puzzleX + (Math.round((xx - puzzleX) / changeSizeImageWidth)) * changeSizeImageWidth;
                                    mc.y = puzzleY + (Math.round((yy - puzzleY) / changeSizeImageHeight)) * changeSizeImageHeight;

                                    // Не выходить за пределы поля
                                    if (mc.x > puzzleRight) mc.x -= changeSizeImageWidth;
                                    if (mc.x < puzzleLeft) mc.x += changeSizeImageWidth;
                                    if (mc.y < puzzleTop) mc.y += changeSizeImageHeight;
                                    if (mc.y > puzzleBottom) mc.y -= changeSizeImageHeight;

                                    //Узнать местоположение в матрице(куда ставится)
                                    let lineX, lineY;
                                    lineX = Math.round((mc.x - puzzleX) / changeSizeImageWidth) - Math.round(freeSpace.faultLeft / changeSizeImageWidth);
                                    lineY = Math.round((mc.y - puzzleY) / changeSizeImageHeight) - Math.round(freeSpace.faultTop / changeSizeImageHeight);

                                    //piecesArrayObj[freeSpace.Left][freeSpace.Top].full = true;
                                    piecesArrayObj[freeSpace.Top][freeSpace.Left].full = true;
                                    piecesArrayObj[freeSpace.Top][freeSpace.Left].i = lineX;
                                    piecesArrayObj[freeSpace.Top][freeSpace.Left].j = lineY;
                                    piecesArrayObj[freeSpace.Top][freeSpace.Left].roll = mc.roll;

                                    if (piecesArrayObj[lineX][lineY].state == false) {
                                        if (isMenu) {
                                            setPositionMenuItems(menuObject[id].position);
                                            menuObject[id].position = null;
                                        }
                                        soundInstance = soundLetGo.play();
                                        if (lineX == freeSpace.Left && lineY == freeSpace.Top && mc.roll == 0){
                                            countPieces++;
                                            changePoints(1);
                                        }
                                        piecesArrayObj[lineX][lineY].state = true;
                                        mc.fuild = true;
                                    } else {
                                        if (isMenu) {
                                            returnInMenu(mc);
                                        }
                                        else {
                                            mc.x = previous.x;
                                            mc.y = previous.y;
                                            piecesArrayObj[freeSpace.Top][freeSpace.Left].i = previous.lineX;
                                            piecesArrayObj[freeSpace.Top][freeSpace.Left].j = previous.lineY;
                                            piecesArrayObj[freeSpace.Top][freeSpace.Left].full = previous.full;
                                            if (previous.right == true){
                                                countPieces++;
                                                changePoints(1);
                                            }
                                            if (mc.fuild == true) {
                                                piecesArrayObj[previous.lineX][previous.lineY].state = true;
                                            }
                                        }
                                    }

                                    mc.addTo(mc.parent);
                                    mc.mouseChildren = false;

                                    //label.text = "Пазлы " + countPieces + "/" + totalPieces;
                                    //label.text = '';

                                    if (countPieces == totalPieces) {
                                        endRound();
                                    }
                                    stage.update();

                                } else {
                                    if (isMenu) {
                                        returnInMenu(mc);
                                    }
                                    piecesArrayObj[freeSpace.Top][freeSpace.Left].full = false;
                                    mc.fuild = false;
                                }
                                function returnInMenu(mc) {
                                    //zog("Обратно");
                                        mc.scaleX = sizeMenuPuzzle / pieceWidth;
                                        mc.scaleY = sizeMenuPuzzle / pieceWidth;
                                        con.removeChild(mc);
                                        footer.addChild(mc);
                                        mc.location = "menu";
                                        menuObject[last.id].location = "menu";
                                        mc.x = last.x;
                                        mc.y = last.y;
                                        stage.update();
                                }
                                //zog(mc);
                            }
                            // Переворот
                            else {
                                if (mc.slide == false && setRoll) {
                                    mc.x = previous.x;
                                    mc.y = previous.y;
                                    rolling(mc, 90);
                                    let freeSpace = space(mc);
                                    piecesArrayObj[freeSpace.Top][freeSpace.Left].roll = mc.roll;

                                    if (previous.lineX == freeSpace.Left && previous.lineY == freeSpace.Top && mc.roll == 0){
                                        countPieces++;
                                        changePoints(1);
                                    }
                                    if (mc.fuild == true) {
                                        piecesArrayObj[previous.lineX][previous.lineY].state = true;
                                    }
                                    //label.text = "Пазлы " + countPieces + "/" + totalPieces;
                                    //label.text = '';
                                    if (countPieces == totalPieces) {
                                        endRound();
                                    }
                                    stage.update();
                                }
                                //zog(mc);
                            }
                        }
                    });
                    
                    context.setStrokeStyle(0.2, "round");
                    let commandi1 = context.beginStroke(createjs.Graphics.getRGB(0, 0, 0)).command;
                    
                    //
                    let pieceImage = imageObj;

                    let m = new createjs.Matrix2D();
                    m.translate(0, 0);
                    m.scale(imageObj.width / imageObj.image.width, imageObj.height / imageObj.image.height);
                    let commandi = context.beginBitmapFill(pieceImage.image, "no-repeat", m).command;

                    context.moveTo(offsetX, offsetY);



                    if (j != 0 && setType == 1) {
                        context.lineTo(offsetX + 3 * x8, offsetY);
                        if (tileObj.up == 1) {
                            context.curveTo(offsetX + 2 * x8, offsetY - 2 * y8, offsetX + 4 * x8, offsetY - 2 * y8);
                            context.curveTo(offsetX + 6 * x8, offsetY - 2 * y8, offsetX + 5 * x8, offsetY);
                        } else {
                            context.curveTo(offsetX + 2 * x8, offsetY + 2 * y8, offsetX + 4 * x8, offsetY + 2 * y8);
                            context.curveTo(offsetX + 6 * x8, offsetY + 2 * y8, offsetX + 5 * x8, offsetY);
                        }
                    }
                    context.lineTo(offsetX + pieceWidth, offsetY);
                    if (i != horizontalPieces - 1 && setType == 1) {
                        context.lineTo(offsetX + 8 * x8, offsetY + 3 * y8);
                        if (tileObj.right == 1) {
                            context.curveTo(offsetX + 10 * x8, offsetY + 2 * y8, offsetX + 10 * x8, offsetY + 4 * y8);
                            context.curveTo(offsetX + 10 * x8, offsetY + 6 * y8, offsetX + 8 * x8, offsetY + 5 * y8);
                        } else {
                            context.curveTo(offsetX + 6 * x8, offsetY + 2 * y8, offsetX + 6 * x8, offsetY + 4 * y8);
                            context.curveTo(offsetX + 6 * x8, offsetY + 6 * y8, offsetX + 8 * x8, offsetY + 5 * y8);
                        }
                    }
                    context.lineTo(offsetX + pieceWidth, offsetY + pieceHeight);
                    if (j != verticalPieces - 1 && setType == 1) {
                        context.lineTo(offsetX + 5 * x8, offsetY + 8 * y8);
                        if (tileObj.down == 1) {
                            context.curveTo(offsetX + 6 * x8, offsetY + 10 * y8, offsetX + 4 * x8, offsetY + 10 * y8);
                            context.curveTo(offsetX + 2 * x8, offsetY + 10 * y8, offsetX + 3 * x8, offsetY + 8 * y8);
                        } else {
                            context.curveTo(offsetX + 6 * x8, offsetY + 6 * y8, offsetX + 4 * x8, offsetY + 6 * y8);
                            context.curveTo(offsetX + 2 * x8, offsetY + 6 * y8, offsetX + 3 * x8, offsetY + 8 * y8);
                        }
                    }
                    context.lineTo(offsetX, offsetY + pieceHeight);
                    if (i != 0 && setType == 1) {
                        context.lineTo(offsetX, offsetY + 5 * y8);
                        if (tileObj.left == 1) {
                            context.curveTo(offsetX - 2 * x8, offsetY + 6 * y8, offsetX - 2 * x8, offsetY + 4 * y8);
                            context.curveTo(offsetX - 2 * x8, offsetY + 2 * y8, offsetX, offsetY + 3 * y8);
                        } else {
                            context.curveTo(offsetX + 2 * x8, offsetY + 6 * y8, offsetX + 2 * x8, offsetY + 4 * y8);
                            context.curveTo(offsetX + 2 * x8, offsetY + 2 * y8, offsetX, offsetY + 3 * y8);
                        }
                    }
                    context.lineTo(offsetX, offsetY);

                    s.regX = offsetX;
                    s.regY = offsetY;
                    s.slide = false;
                    //s.addTo(con);
                    s.addTo(footer);

                    let fill = new createjs.Graphics.Fill("red");

                    // Перемещение по карте
                    /*s.animate({
                        obj: {
                            x: rand(0, frame.width - pieceWidth),
                            y: rand(0, frame.height - pieceHeight)
                        },
                        time: 500
                    });*/

                    function randomInteger(min, max) {
                      let rand = min + Math.random() * (max - min);
                      return Math.round(rand);
                    }

                    // Поставить в нижнее меню и уменьшить
                    let randomRoll
                    if (setRoll) randomRoll = randomInteger(0,3);
                    else randomRoll = 0;
                    rolling(s, 90 * randomRoll);
                    s.roll = randomRoll;
                    s.scaleX = sizeMenuPuzzle / pieceWidth;
                    s.scaleY = sizeMenuPuzzle / pieceWidth;
                    
                    s.x = menuObject[n].position * sizeMenuPuzzle + marginMenu * menuObject[n].position + marginFirstElem;
                    s.y = (heightLowerMenu - sizeMenuPuzzle) / 2;

                    if (s.roll == 1) s.x += sizeMenuPuzzle;
                    if (s.roll == 3) s.y += sizeMenuPuzzle;
                    if (s.roll == 2) {
                        s.x += sizeMenuPuzzle;
                        s.y += sizeMenuPuzzle;
                    }

                    menuObject[n].object = s;
                    menuObject[n].location = "menu";
                    menuObject[n].countInMenu = 0;
                    s.location = "menu";

                    function rolling(mc, rot) {

                        let x = mc.regX, y = mc.regY;
                        mc.regX = mc.regX + changeSizeImageWidth / 2;
                        mc.regY = mc.regY + changeSizeImageHeight / 2;
                        mc.rotation += rot;
                        if (mc.rotation % 360 == 90) {
                            mc.x += changeSizeImageWidth;
                            mc.roll = 1;
                        }
                        if (mc.rotation % 360 == 180) {
                            mc.y += changeSizeImageHeight;
                            mc.roll = 2;
                        }
                        if (mc.rotation % 360 == 270) {
                            mc.x -= changeSizeImageWidth;
                            mc.roll = 3;
                        }
                        if (mc.rotation % 360 == 0) {
                            mc.y -= changeSizeImageHeight;
                            mc.roll = 0;
                        }

                        mc.regX = x;
                        mc.regY = y;
                    }
                }
            }

            resizeObject(width - margin * 2, height - heightLowerMenu - margin * 2 - sizeWatch, stage, margin, sizeWatch);

            con.addTo(stage);

            stage.update();

            generateGame.StartGame = StartGame;

            function stageUpdate() {
                stage.update();
            }
            setTimeout(stageUpdate, 500);
            stage.update(); // this is needed to show any changes
        }
        updateSizeGame();
    }); // end of ready
}

function active(i, j, bool, visible) {
    for (i = 0; i < verticalPieces; i++) 
        for (j = 0; j < horizontalPieces; j++) {
            piecesArrayObj[i][j].obj.mouseEnabled = bool;
            //piecesArrayObj[i][j].obj.visible = visible;
            piecesArrayObj[i][j].obj.animate({alpha: visible},500);
        }
}

function setPositionMenuItems(position) {
	console.log(Math.round((width - marginFirstElem) / (sizeMenuPuzzle + marginMenu)), widthLowerMenu);
	//console.log(Math.round((width - marginFirstElem) / (sizeMenuPuzzle + marginMenu)));
	countMenuPuzzle--;
	
	//if (widthLowerMenu + (sizeMenuPuzzle + marginMenu) > width) return;
	//console.log(widthLowerMenu + (sizeMenuPuzzle + marginMenu), width);
    
    for (i = 0; i < menuObject.length; i++) {
        if (menuObject[i].position > position && menuObject[i].position != null) {
            menuObject[i].position--;
            setPositionPuzzleMenu(i);
        }
    }

    if (Math.round((width - marginFirstElem) / (sizeMenuPuzzle + marginMenu)) > countMenuPuzzle + 1 ) return;

    widthLowerMenu += (sizeMenuPuzzle + marginMenu);
    if (footer.regX > sizeMenu - widthLowerMenu) {
        footer.regX = sizeMenu - widthLowerMenu;
    }
    if (footer.regX < 0) footer.regX = 0;
    stage.update();
}

function addPuzzleInMenu(elem, id) {

    let position = Math.round((footer.regX + elem.x - marginFirstElem) / (sizeMenuPuzzle + marginMenu))
    if (position < 0) position = 0;

    countMenuPuzzle++;

    if (Math.round((width - marginFirstElem) / (sizeMenuPuzzle + marginMenu)) > countMenuPuzzle) {
    	menuObject[id].position = countMenuPuzzle;
    	setPositionPuzzleMenu(id);
    } else {
	    for (i = 0; i < menuObject.length; i++) {
	        if (menuObject[i].position >= position && menuObject[i].position != null) {
	            menuObject[i].position++;
	            setPositionPuzzleMenu(i);
	        }
	    }
    	menuObject[id].position = position;
	    setPositionPuzzleMenu(id);
	    widthLowerMenu -= (sizeMenuPuzzle + marginMenu);
    }
    stage.update();
}

function setPositionPuzzleMenu (i) {
    menuObject[i].object.x = menuObject[i].position * sizeMenuPuzzle + marginMenu * menuObject[i].position + marginFirstElem;
    menuObject[i].object.y = (heightLowerMenu - sizeMenuPuzzle) / 2;;

    if (menuObject[i].object.roll == 1) menuObject[i].object.x += sizeMenuPuzzle;
    if (menuObject[i].object.roll == 3) menuObject[i].object.y += sizeMenuPuzzle;
    if (menuObject[i].object.roll == 2) {
        menuObject[i].object.x += sizeMenuPuzzle;
        menuObject[i].object.y += sizeMenuPuzzle;
    }
}