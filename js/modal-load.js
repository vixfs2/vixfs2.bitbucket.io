/*$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");},
     ajaxStop: function() { $body.removeClass("loading"); }    
});*/

function modalResize() {
	var screen;

	if ($(window).width() < $(window).height()*0.8) 
  		screen = $(window).width();
  	else 
  		screen = $(window).height()*0.8;

	$('.modal_content').width(screen * 0.84);
	// Изобраение брейнсканнера и текст
	$('.brainIcon').height($('.modal_icon').height()*0.8);
    $('.modal_text, .button_modal').css('font-size', $('.modal_icon').height() * 0.11 + "px");
}

$(function(){
	$(window).resize(function(){
	  modalResize();
	});
});

function showModal()
{
  	$(".modal-load").css('display', "flex");
  	modalResize();
}

function closeModal(){
	$(".modal-load").css('display', "none");
}

function closeAndStart(){
	closeModal();
	newSessionGame();
}