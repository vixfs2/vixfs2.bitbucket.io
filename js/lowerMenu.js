var moveMenuTimer;
var background;
let widthLowerMenu;
function resizeObject(width, height, stage, margin, sizeWatch) {
	if (width > height) {
		imageObj.height = height;
        if (typeof mainImage != "undefined")
            mainImage.height = height;
		coeff = height / initialValueHeight;
	}
	else {
		imageObj.width = width;
        if (typeof mainImage != "undefined")
            mainImage.width = width;
		coeff = width / initialValueWidth;
	}

    imageWatch.x = (width + - margin * 2) / 2 + imageObj.width / 2 - sizeWatch / 2;

	// Отступы
	changeSizeImageHeight = pieceWidth * coeff;
	changeSizeImageWidth = pieceWidth * coeff;
	indentImage = pieceWidth - pieceWidth * coeff;
	puzzleX = ((width + margin * 2) - imageWidth * coeff)/2;
    puzzleY = ((height + margin * 2) - imageHeight * coeff)/2 + sizeWatch;
    whitebackground.x = puzzleX;
    whitebackground.y = puzzleY;
    whitebackground.scaleX = coeff;
    whitebackground.scaleY = coeff;
    if (typeof leftArrowBackground != "undefined")
        leftArrowBackground.y = height + margin * 2 + sizeWatch;
    if (typeof rightArrowBackground != "undefined") {
        rightArrowBackground.x = width - 40 + margin * 2;
        rightArrowBackground.y = height + margin * 2 + sizeWatch;
    }
    if (typeof mainImage != "undefined")
        mainImage.pos(puzzleX,puzzleY);


    imageObj.pos(puzzleX,puzzleY);

    for (j = 0; j < verticalPieces; j++) 
        for (i = 0; i < horizontalPieces; i++) 
        	updatePuzzleSize(j, i) 
	stage.update();
}

function updatePuzzleSize(j, i) {
    // Изменение картинок
    if ((menuObject[j  + i * verticalPieces].location) != "menu") {
        piecesArrayObj[i][j].obj.scaleX = coeff;
        piecesArrayObj[i][j].obj.scaleY = coeff;
        // Внутри квадрата
        if (piecesArrayObj[i][j].full == true) {
            piecesArrayObj[i][j].obj.x = puzzleX + piecesArrayObj[i][j].i * changeSizeImageWidth;
            piecesArrayObj[i][j].obj.y = puzzleY + piecesArrayObj[i][j].j * changeSizeImageHeight;
            if (piecesArrayObj[i][j].roll == 1) piecesArrayObj[i][j].obj.x += changeSizeImageWidth;
            if (piecesArrayObj[i][j].roll == 3) piecesArrayObj[i][j].obj.y += changeSizeImageHeight;
            if (piecesArrayObj[i][j].roll == 2) {
                piecesArrayObj[i][j].obj.x += changeSizeImageWidth;
                piecesArrayObj[i][j].obj.y += changeSizeImageHeight;
            }
        }
    }
    //}
}

function lowerMenu(height, width, container, stage) {
    for (var i = 0; i < horizontalPieces * verticalPieces; i++) {
        menuObject[i] = {};
        menuObject[i].id = i;
        menuObject[i].position = i;
    }

    shuffle(menuObject);

    // Перемешать элементы
    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        while (0 !== currentIndex) {

            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            temporaryValue = array[currentIndex].position;
            array[currentIndex].position = array[randomIndex].position;
            array[randomIndex].position = temporaryValue;
        }

        return array;
    }

    // Фон
    background = new Shape();
    if (sizeMenu > 1800)
        background.graphics.beginFill(colorMenu).drawRect(0, 0, sizeMenu, height);
    else 
        background.graphics.beginFill(colorMenu).drawRect(0, 0, 1800, height);
    container.addChild(background);

    container.addTo(stage);
    stage.update();

    var clickX, regX, mouse, swipeX, timer;

    //Событие при нажатии
    container.on("mousedown", function(evt) {
        clearTimeout(moveMenuTimer);
        clickX = widthLowerMenu - evt.stageX;
        regX = container.regX;
        mouse = evt;
        timer = setTimeout(swipe, false);
        stage.update();
    });

    //Событие при перемещении
    container.on("pressmove", function(evt) {
        // Границы
        if (enable) {
            mouse = evt;
            if (regX + (widthLowerMenu - evt.stageX) - clickX >=0 && regX + (widthLowerMenu - evt.stageX) - clickX <= sizeMenu - widthLowerMenu)
                container.regX = regX + (widthLowerMenu - evt.stageX) - clickX;
            else 
                if (regX + (widthLowerMenu - evt.stageX) - clickX < 0) {
                    regX = 0;
                    clickX = widthLowerMenu - evt.stageX;
                }
                else {
                    regX = sizeMenu - widthLowerMenu;
                    clickX = widthLowerMenu - evt.stageX;
                }
            stage.update();
        }
    });

    //Отпустить
    container.on("pressup", function(evt) {
        clearTimeout(timer);
        if (enable && evt.stageY > window.height - heightLowerMenu * 2 / 3) {
            moveMenu(swipeX, evt.stageX);
        }
    });

    // Свайп
    function swipe() {
        if (mouse.stageX != undefined) swipeX = mouse.stageX;
        setTimeout(swipe, 200);
    }

    function moveMenu(before, after) {
        var difference = (before - after) * 5;
        var negative = false;
        if (difference < 0) negative = true;

        moveMenuTimer = setTimeout(move, false);
        function move() {
            if (negative) {
                container.regX -= slow(difference);
                difference = difference + slow(difference);
                // Граница слева
                if (container.regX < 0) {
                    container.regX = 0;
                    stage.update();
                    return 0;
                }
                if (difference > -1) return 0;
            }
            else {
                container.regX += slow(difference);
                difference = difference - slow(difference);
                // Граница справа
                if (container.regX > sizeMenu - widthLowerMenu) {
                    container.regX = sizeMenu - widthLowerMenu;
                    stage.update();
                    return 0;
                }
                if (difference < 1) return 0;
            }

            moveMenuTimer = setTimeout(move, 30);
            stage.update();
        }

        stage.update();
    }

    function slow(difference) {
        difference = Math.abs(difference);
        return Math.sqrt(2 * difference);
    }
}
